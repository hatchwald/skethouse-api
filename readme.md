## EXPRESS JS API
> APIs
With a myriad of HTTP utility methods and middleware at your disposal, creating a robust API is quick and easy.

project API for transaction data
for more detail to use Express check [documentation](https://expressjs.com/)

## step to use this API
1. install dependicies with `npm install`
2. to start site use `npm run start`
3. but if you want development view use `npm run dev`
4. your api will serve on `http://localhost:3000`


## list Route
- http://localhost/comment (POST)
- http://localhost/comment (DELETE)
- http://localhost/comment/alldata (GET)


made with ExpressJs by [hatchwald](https://gitlab.com/hatchwald)
you can found me too on [github](https://github.com/hatchwald)  