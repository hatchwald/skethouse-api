var express = require('express');
var router = express.Router();
const fs = require("fs");
const dbFile = "./db/database.db";
const exists = fs.existsSync(dbFile);
const sqlite3 = require("sqlite3").verbose();
const db = new sqlite3.Database(dbFile);
const { Validator, ValidationError } = require("express-json-validator-middleware")
const { validate } = new Validator();

// default seed db
db.serialize(() => {
    if (!exists) {
        db.run(
            "CREATE TABLE comment (id INTEGER PRIMARY KEY AUTOINCREMENT,name TEXT ,comments TEXT)"
        );
        console.log("New table Comment created!");

        // insert default data
        db.serialize(() => {
            db.run(
                'INSERT INTO comment (name,comments) VALUES ("jake","Find and count some sheep"), ("finn","Climb a really tall mountain"), ("beemo","Wash the dishes")'
            );
        });
    } else {
        console.log('Database "comment" ready to go!');
        db.each("SELECT * from comment", (err, row) => {
            if (row) {
                console.log(`record: ${row.name} comment : ${row.comments}`);
            }
        });
    }
});

// schema data
const CommentSchema = {
    type: "object",
    required: ["name", "comments"],
    properties: {
        name: {
            type: "string",
            minLength: 1
        },
        comments: {
            type: "string",
            minLength: 8
        }
    }
}

function validationErrorHandler(err, req, res, next) {
    if (res.headersSent) {
        return next(err)
    }

    const isValidationErr = err instanceof ValidationError;
    if (!isValidationErr) {
        return next(err)
    }

    res.status(400).json({
        errors: err.validationErrors
    })

    next()
}

router.post("/", validate({ body: CommentSchema }),
    function createCommentHandler(req, res, next) {
        const bodydata = req.body
        db.run(`INSERT INTO comment (name,comments) VALUES ("${bodydata.name}","${bodydata.comments}")`, err => {
            if (err) {
                res.send(err)
                console.log(err)
            } else {
                res.send("success insert data")
            }
        })
        // res.json(bodydata)

        // next()
    }
)

router.use(validationErrorHandler)

/* GET users listing. */
router.get('/alldata', function (req, res, next) {
    db.all("SELECT * FROM comment", (err, rows) => {
        res.send(JSON.stringify(rows));
    });
});

router.delete("/", (req, res) => {
    db.run("DELETE FROM comment", err => {
        if (err) {
            res.send("error happen at cleared data")
        } else {
            res.send("all data was cleared !")
        }
    })
})

module.exports = router;
